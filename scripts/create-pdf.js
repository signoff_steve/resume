const puppeteer = require('puppeteer');
const path = require('path');
const pathToFile = path.resolve(__dirname, '../build/cv.html');

(async () => {
  const browser = await puppeteer.launch({
    args: ['--no-sandbox', '--headless']
  });
  const page = await browser.newPage();

  await page.goto(`file:///${pathToFile}`);
  await page.pdf({
    path: 'build/cv.pdf',
    format: 'A4',
    printBackground: true,
    displayHeaderFooter: false,
    margin: {
      top: '10mm',
      right: '10mm',
      bottom: '10mm',
      left: '10mm',
    }
  });

  await browser.close();
})();
